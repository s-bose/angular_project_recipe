export class recipeQueryModel {
    category ?: string;
    area ?: string;
    mainIngredient ?: string;
};

export enum Tags {
    category,
    area,
};